package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class BookActivity extends AppCompatActivity {

    Book book = new Book();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        final BookDbHelper dbHelper = new BookDbHelper(this);
        final Bundle extras = getIntent().getExtras();

        if (extras != null){

            book = (Book) getIntent().getParcelableExtra("book");

            String title = book.getTitle();
            String authors = book.getAuthors();
            String year = book.getYear();
            String genres = book.getGenres();
            String publisher = book.getPublisher();


            ((EditText) findViewById(R.id.nameBook)).setText(title);
            ((EditText) findViewById(R.id.editAuthors)).setText(authors);
            ((EditText) findViewById(R.id.editYear)).setText(year);
            ((EditText) findViewById(R.id.editGenres)).setText(genres);
            ((EditText) findViewById(R.id.editPublisher)).setText(publisher);

        }


        final Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String title = (String) ((EditText) findViewById(R.id.nameBook)).getText().toString();
                String authors = (String) ((EditText) findViewById(R.id.editAuthors)).getText().toString();
                String year = (String) ((EditText) findViewById(R.id.editYear)).getText().toString();
                String genres = (String) ((EditText) findViewById(R.id.editGenres)).getText().toString();
                String publisher = (String) ((EditText) findViewById(R.id.editPublisher)).getText().toString();


                book.setTitle(title);
                book.setAuthors(authors);
                book.setYear(year);
                book.setGenres(genres);
                book.setPublisher(publisher);

                if (extras != null){
                    dbHelper.updateBook(book);
                }

                else{
                    dbHelper.addBook(book);
                }


                Toast.makeText(BookActivity.this, "Livre Sauvegardé !",
                        Toast.LENGTH_LONG).show();
                finish();
                Intent intent = new Intent(BookActivity.this, MainActivity.class);
                startActivity(intent);
            }

        });



    }

}

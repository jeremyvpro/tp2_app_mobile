package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class BookDbHelper extends SQLiteOpenHelper {

    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";

    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";

    public BookDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(" CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_BOOK_TITLE + " TEXT NOT NULL, " +
                COLUMN_AUTHORS + " TEXT NOT NULL, " +
                COLUMN_YEAR + " TEXT NOT NULL, " +
                COLUMN_GENRES + " TEXT NOT NULL, " +
                COLUMN_PUBLISHER + " TEXT NOT NULL, " +
                "UNIQUE (" + COLUMN_BOOK_TITLE + "," + COLUMN_AUTHORS + ") ON CONFLICT ROLLBACK);"
        );



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }


   /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();

        String title = book.getTitle();
        String authors = book.getAuthors();
        String year = book.getYear();
        String genres = book.getGenres();
        String publisher = book.getPublisher();

        ContentValues values = new ContentValues();
        values.put(COLUMN_BOOK_TITLE, title);
        values.put(COLUMN_AUTHORS, authors);
        values.put(COLUMN_YEAR, year);
        values.put(COLUMN_GENRES, genres);
        values.put(COLUMN_PUBLISHER, publisher);

        long rowID = 0;
        rowID = db.insert(TABLE_NAME, null, values);

        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */
    public int updateBook(Book book) {
        SQLiteDatabase db = this.getWritableDatabase();
	int res;

	    String id = Long.toString(book.getId());
        String title = book.getTitle();
        String authors = book.getAuthors();
        String year = book.getYear();
        String genres = book.getGenres();
        String publisher = book.getPublisher();

        ContentValues values = new ContentValues();
        values.put(COLUMN_BOOK_TITLE, title);
        values.put(COLUMN_AUTHORS, authors);
        values.put(COLUMN_YEAR, year);
        values.put(COLUMN_GENRES, genres);
        values.put(COLUMN_PUBLISHER, publisher);

        String conditionId = _ID + "=" + book.getId();

        res = db.update(TABLE_NAME, values, conditionId, null);
        return res;
    }


    public Cursor fetchAllBooks() {
        SQLiteDatabase db = this.getReadableDatabase();

	Cursor cursor = null;
	cursor = db.query(TABLE_NAME, new String[] {"ROWID AS _id", COLUMN_BOOK_TITLE, COLUMN_AUTHORS}, null, null,null, null, COLUMN_BOOK_TITLE );
        
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public void deleteCity(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_NAME, _ID + "=?", new String[]{cursor.getString(cursor.getColumnIndex(_ID))} );
        db.close();
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

    public static Book cursorToBook(Cursor cursor) {
        Book book = null;
        cursor.moveToFirst();

        book = new Book();
        Long id = cursor.getLong(0);
        String title = cursor.getString(1);
        String authors = cursor.getString(2);
        String year = cursor.getString(3);
        String genres = cursor.getString(4);
        String publisher = cursor.getString(5);

        book.setId(id);
        book.setTitle(title);
        book.setAuthors(authors);
        book.setYear(year);
        book.setGenres(genres);
        book.setPublisher(publisher);



        return book;
    }
}

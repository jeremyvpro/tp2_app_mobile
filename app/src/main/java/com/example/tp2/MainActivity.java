package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ListView bookList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final BookDbHelper dbHelper = new BookDbHelper(this);
        dbHelper.populate();


        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbHelper.fetchAllBooks(),
                new String[] {BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS},
                new int[] {android.R.id.text1, android.R.id.text2 }, 0);

        bookList = (ListView) findViewById(R.id.listBook);

        bookList.setAdapter(adapter);

        registerForContextMenu(bookList);
        bookList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {

                SQLiteDatabase db = dbHelper.getReadableDatabase();


                Cursor cursor = null;

                String conditionId =  "_id="+ Long.toString(id);
                cursor = db.query(BookDbHelper.TABLE_NAME, null,
                        conditionId,null,null,null,null);

                Book book = null;
                if (cursor != null) {

                    book = BookDbHelper.cursorToBook(cursor);
                }

                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                intent.putExtra("book", book);
                startActivity(intent);

            }


        });


        final FloatingActionButton button = findViewById(R.id.buttonAdd);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                startActivity(intent);
            }

       });




    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add("Supprimer");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item){
        super.onContextItemSelected(item);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        if (item.getTitle() == "Supprimer"){

            AdapterView bookList =  findViewById(R.id.listBook);
            Cursor cursor = (Cursor) bookList.getItemAtPosition(info.position);
            final BookDbHelper dbHelper = new BookDbHelper(this);

            if (cursor != null){
                dbHelper.deleteCity(cursor);
            }
            finish();
            startActivity(getIntent());
            Toast toast = Toast.makeText(getApplicationContext(), "Livre supprimé",Toast.LENGTH_SHORT);
            toast.show();


        }

        return super.onContextItemSelected(item);
    }



}
